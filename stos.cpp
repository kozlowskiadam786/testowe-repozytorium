#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Stos
{
    int pojemnosc = 1000, pozycja = -1;
    int *stos;
};

Stos init()
{
    Stos temp;
    temp.stos = new int[temp.pojemnosc];

    for (int i = 0; i < temp.pojemnosc; i++)
    {
        temp.stos[i] = 0;
    }

    return temp;
}

void destroy(Stos temp)
{
    delete[] temp.stos;
}

void push(Stos *temp, int wartosc)
{
    if (temp->pozycja < temp->pojemnosc - 1)
    {
        temp->pozycja++;
        temp->stos[temp->pozycja] = wartosc;
    }
}

int pop(Stos *temp)
{
    int wartosc = -1;

    if (temp->pozycja > -1)
    {
        wartosc = temp->stos[temp->pozycja];
        temp->stos[temp->pozycja] = 0;
        temp->pozycja--;
    }

    return wartosc;
}

int top(Stos temp)
{
    if (temp.pozycja > -1) return temp.stos[temp.pozycja];
    else return -1;
}

bool empty(Stos temp)
{
    if (temp.pozycja < 0) return true;
    else return false;
}

bool full(Stos temp)
{
    if (temp.pozycja >= temp.pojemnosc - 1) return true;
    else return false;
}

int main()
{
    srand(time(NULL));

    Stos s = init();

    int iloscLiczb = rand() % 50 + 1;

    for (int i = 0; i < iloscLiczb; i++)
    {
        push(&s, rand() % 20 + 1);
    }

    while (!empty(s))
    {
        std::cout << pop(&s) << " ";
    }
    
    destroy(s);

    return 0;
}